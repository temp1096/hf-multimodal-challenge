# Notes
## Intro
In this document I'll detail my approach to the different stages, and some additional ideas I had. The files in the notes/ directory are more scratch-pad thoughts.

## Dataset preparation
My approach to this portion was to use an image captioning dataset to produce sample sentences. For each ImageNet class, I searched for image captions containing the class name (and no other class name) to augment the dataset. This produced many sentences that looked to be fluent, and described an actual image which seemed useful for this task.
(more details in `notes/generate_data.md`)

To build the dataset that would be used to train the model, I ran the input sentences through a pre-trained distilbert model to generate the inputs. Here, I tried a few ideas, using the CLS token, pooling over the sentence embeddings, and using the second-last hidden layer instead of the last layer. With more time I might explore a few more ideas, such as inputting the whole (512x768) sentence embedding into my mapping network. For the target outputs, I used the class embeddings from the BigGAN model.
(more details in `notes/build_dataset.md`)

## Model architecture & training
I tried a few different model shapes and sizes. Because the inputs and outputs of the mapping network were relatively small, 768 and 128 dimensional vectors respectively, I hoped a small model would suffice. To begin with I tried a single fully connected layer, which didn't work well. Then, I tried inserting some hidden layers, to increase the number of parameters in the network.

I split the dataset into training and testing, and built my training loop using pytorch-lightning for simplicity. This allowed me to easily add things like checkpointing, and early stopping.

Overfitting didn't seem to be a big issue here as the validation loss and training loss remained fairly close to one another, so perhaps I could have increased the model size further, but I was constrained by available computational resources.

## Running the code
1. In a virtual env, `pip install -r requirements.txt`
2. Run `download_captions.sh` to download an image caption dataset (33MB, nothing too crazy since it's just text)
3. Run `python prepare_data.py`, then `python build_dataset.py`, then `python train_model.py`
4. Finally, run `python demo.py` passing as `--checkpoint_path` the path to the .ckpt file produced by the training script. This script will prompt you for text you wish to generate.

## Conclusion
In the end, after trying a few different ideas, the model still wasn't quite able to generate cohesive images, even for words directly corresponding to the ImageNet class labels. I was able to get the loss function to decrease during training, suggesting something was kind of working (?) but unfortunately, not enough.

Some sample images generated are in the `outputs` directory. Enjoy! This was a fun and interesting challenge, thank you!

Note: I found a couple of small typos in the README, I've pushed them as well to this repository.
