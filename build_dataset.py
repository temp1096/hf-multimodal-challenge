import argparse
from pathlib import Path

import torch
import tqdm
from pytorch_pretrained_biggan import BigGAN
from transformers import AutoModel, AutoTokenizer


def load_lm(pretrained_model_name):
    model = AutoModel.from_pretrained(pretrained_model_name)

    return model


def load_examples(input_dir):
    tokens_tensor = torch.load(input_dir / "tokens_tensor.bin")
    labels_tensor = torch.load(input_dir / "labels_tensor.bin")

    return tokens_tensor, labels_tensor


def load_biggan(biggan_model_name):
    model = BigGAN.from_pretrained(biggan_model_name)

    return model


def generate_inputs(lm, tokens):
    """
    Loop over tokens in batches and compute embeddings
    Need to go in batches here because the BERT model is
    too heavy.
    """
    batch_size = 4
    lm_embeddings = []

    if torch.cuda.is_available():
        lm = lm.to('cuda')
        tokens = tokens.to('cuda')

    with torch.no_grad():
        for i in tqdm.tqdm(range(0, tokens.size()[0], batch_size)):
            batch = tokens[i : i + batch_size, :]
            lm_embeddings.append(lm(batch, output_hidden_states=True).hidden_states[-2])

    return torch.cat(lm_embeddings)


def generate_targets(image_model, labels):
    return image_model.embeddings(torch.nn.functional.one_hot(labels).float())


def save_dataset(inputs, targets, output_dir):
    torch.save(inputs, output_dir / "inputs.bin")
    torch.save(targets, output_dir / "targets.bin")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pretrained_lm_model_name",
        default="distilbert-base-uncased",
        type=str,
        help="Pretrained language model",
    )
    parser.add_argument(
        "--pretrained_biggan_model_name",
        default="biggan-deep-128",
        type=str,
        help="Pretrained biggan model",
    )
    parser.add_argument("--input_dir", type=Path, default="data")
    parser.add_argument("--output_dir", type=Path, default="data")
    args = parser.parse_args()

    language_model = load_lm(args.pretrained_lm_model_name)
    tokens, labels = load_examples(args.input_dir)
    image_model = load_biggan(args.pretrained_biggan_model_name)
    inputs = generate_inputs(language_model, tokens)
    targets = generate_targets(image_model, labels)
    save_dataset(inputs, targets, args.output_dir)
