import argparse
from pathlib import Path

import torch

from train_model import MappingNetwork
from run_model import text_to_image

def load_model(checkpoint_path):
    return MappingNetwork.load_from_checkpoint(checkpoint_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--checkpoint_path", type=Path)
    args = parser.parse_args()

    mapping_model = load_model(args.checkpoint_path)

    while True:
        text = input("What would you like to generate? ")
        text_to_image(text, mapping_model)
