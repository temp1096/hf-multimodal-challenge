# Helper script to download VizWiz image captioning dataset
wget http://ivc.ischool.utexas.edu/VizWiz_final/caption/annotations.zip
unzip annotations.zip
rm annotations.zip
