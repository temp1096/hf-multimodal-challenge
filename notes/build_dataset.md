Ideas:
- Use second last hidden state layer instead of last, sometimes it works better
- Instead of using the CLS token, maybe use the token corresponding to the class? e.g. in a sentence like "I saw a fish", give the embedding of the token "fish" as input to the model

Notes
- Got stuck for a while running out of memory, before I remembered to use `with torch.no_grad`
