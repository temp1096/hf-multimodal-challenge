Ideas:
- Use naturally occurring sentences from a text dataset containing the image class labels
    - Or even use an image captioning dataset!
    - Potential challenge: These sentences could be too specific
- Use several different sentence templates, and score them with a language model to choose the most probable one
