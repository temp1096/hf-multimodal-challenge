- Basic baseline model - simple fully-connected layer using the CLS token as input
    - Results were not great, loss didn't really decrease, maybe too simple of a model
- Next step: multi-layer network to learn mapping
    - Trying 2 hidden layers with ReLU activation in between
    - Still had poor results, loss not decreasing
- Test with original dataset, excluding my mined sentences
    - Loss still not decreasing, same results more or less
- Try shuffling training set with the dataloader, still no luck
- Try mining 10 sentences per label class, instead of just 5, seems to work a bit!
- Tried a larger batch size (was originally just using 1, can easily get up to 32 or 64 with such a small model)
- Trained overnight with a larger batch and a smaller learning rate, and loss seemed to be decreasing reasonably, but ultimately only went down about 25% from the random starting point, which I doubt is enough.
- Generated output images from this checkpoint, giving it simple prompts coming directly from the class labels, images didn't seem to reflect their class at all
    - Even with a higher truncation value to increase the quality, images still don't seem right
- Tried to pool over the entire LM output, instead of just using the CLS token
- Tried adding a loss term to push the outputs further away from other classes, though I'm not sure I tuned the relative weight in the loss well. Did see something that maybe looked dog-ish generated, but maybe I'm just seeing what I want to.

More ideas:
- Fine-tune BERT, instead of just learning the mapping layer
