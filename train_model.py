import argparse
from pathlib import Path

import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from torch import nn
from torch.utils.data import DataLoader, TensorDataset, random_split


class MappingNetwork(pl.LightningModule):
    def __init__(self, input_dim=768, hidden_dim=128, output_dim=128):
        super().__init__()
        self.strategy = 'max'
        self.mapping = nn.Sequential(
            nn.Linear(input_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, hidden_dim),
            nn.ReLU(),
            nn.Linear(hidden_dim, output_dim)
        )
        self.loss = nn.MSELoss()

    def forward(self, x):
        x = self.pool(torch.unsqueeze(x, 0)).squeeze(0)
        output = self.mapping(x)

        return output

    def pool(self, x):
        if self.strategy == 'CLS':
            return x[:, 0]
        elif self.strategy == 'max':
            return torch.max(x, dim=1, keepdim=True).values

    def training_step(self, batch, batch_idx):
        x, y = batch
        # Just take CLS token of input tokens
        x = self.pool(x)
        z = self.mapping(x)
        loss = self.loss(z, y)
        self.log("train_loss", loss)

        return loss

    def validation_step(self, batch, batch_idx):
        with torch.no_grad():
            x, y = batch
            # Just take CLS token of input tokens
            x = x[:, 0, :]
            z = self.mapping(x)
            loss = self.loss(z, y)
            self.log("val_loss", loss)

        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-5)

        return optimizer


def load_dataset(input_dir):
    inputs = torch.load(input_dir / "inputs.bin")
    targets = torch.load(input_dir / "targets.bin")[:inputs.shape[0]]

    return TensorDataset(inputs, targets)


def train(input_dir, output_dir):
    dataset = load_dataset(input_dir)
    train_size = int(len(dataset) * 0.9)
    train, val = random_split(dataset, [train_size, len(dataset) - train_size])

    mapping = MappingNetwork()
    trainer = pl.Trainer(callbacks=[EarlyStopping(monitor="val_loss")], gpus=1)
    trainer.fit(mapping, DataLoader(train, batch_size=64, shuffle=True), DataLoader(val, batch_size=64))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir", type=Path, default="data")
    parser.add_argument("--output_dir", type=Path, default="output")
    args = parser.parse_args()

    train(args.input_dir, args.output_dir)
